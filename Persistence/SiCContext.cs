using Microsoft.EntityFrameworkCore;

namespace SiC_GC.Models
{
    public class SiCContext : DbContext
    {
        public SiCContext(DbContextOptions<SiCContext> options) : base(options) { }

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductPrice> ProductPrices { get; set; }
        public DbSet<Catalog> Catalogs { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Collection> Collections { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<MaterialFinish> MaterialFinishes { get; set; }
        public DbSet<MaterialFinishPrice> MaterialFinishPrices { get; set; }
        public DbSet<Dimension> Dimensions { get; set; }
        public DbSet<ProductMaterialFinish> ProductMaterialFinishes { get; set; }
        public DbSet<ProductRelation> ProductRelations { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            

            modelBuilder.Entity<ProductMaterialFinish>()
                .HasKey(pmf => new { pmf.ProductId, pmf.MaterialFinishId });

            modelBuilder.Entity<ProductMaterialFinish>()
                .HasOne(pmf => pmf.Product)
                .WithMany(p => p.possibleMaterialFinishes)
                .HasForeignKey(pmf => pmf.ProductId);

            modelBuilder.Entity<ProductMaterialFinish>()
                .HasOne(pmf => pmf.MaterialFinish)
                .WithMany(mf => mf.ProductMaterialFinishes)
                .HasForeignKey(pmf => pmf.MaterialFinishId);

            modelBuilder.Entity<ProductRelation>()
                        .HasKey(e => e.productRelationId);

            modelBuilder.Entity<ProductRelation>()
                .HasOne(d => d.ParentProduct)
                .WithMany()
                .HasForeignKey(d => d.ParentProductId);


            modelBuilder.Entity<ProductRelation>()
                .HasOne(d => d.ChildProduct)
                .WithMany()
                .HasForeignKey(d => d.ChildProductId);


        }
    }



}