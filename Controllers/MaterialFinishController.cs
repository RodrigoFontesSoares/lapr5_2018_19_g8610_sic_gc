using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Models;

namespace SiC_GC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaterialFinishController : ControllerBase
    {
        private readonly SiCContext _context;

        public MaterialFinishController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/MaterialFinish
        [HttpGet]
        public IEnumerable<MaterialFinish> GetMaterialFinishes()
        {
            return _context.MaterialFinishes.Include(mf => mf.material);
        }

        // GET: api/MaterialFinish/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMaterialFinish([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var materialFinish = await _context.MaterialFinishes
                                                .Include(mf => mf.material)
                                                .FirstOrDefaultAsync(m => m.MaterialFinishId == id);

            if (materialFinish == null)
            {
                return NotFound();
            }

            return Ok(materialFinish);
        }

        // PUT: api/MaterialFinish/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMaterialFinish([FromRoute] int id, [FromBody] MaterialFinish materialFinish)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != materialFinish.MaterialFinishId)
            {
                return BadRequest();
            }

            _context.Entry(materialFinish).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaterialFinishExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MaterialFinish
        [HttpPost]
        public async Task<IActionResult> PostMaterialFinish([FromBody] MaterialFinish materialFinish)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var material = await _context.Materials.FindAsync(materialFinish.material.MaterialId);
            materialFinish.material = material;

            _context.MaterialFinishes.Add(materialFinish);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMaterialFinish", new { id = materialFinish.MaterialFinishId }, materialFinish);
        }

        // DELETE: api/MaterialFinish/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMaterialFinish([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var materialFinish = await _context.MaterialFinishes.FindAsync(id);
            if (materialFinish == null)
            {
                return NotFound();
            }

            _context.MaterialFinishes.Remove(materialFinish);
            await _context.SaveChangesAsync();

            return Ok(materialFinish);
        }

        private bool MaterialFinishExists(int id)
        {
            return _context.MaterialFinishes.Any(e => e.MaterialFinishId == id);
        }
    }
}