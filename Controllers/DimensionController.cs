using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Models;

namespace SiC_GC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DimensionController : ControllerBase
    {
        private readonly SiCContext _context;

        public DimensionController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/Dimension
        [HttpGet]
        public IEnumerable<Dimension> GetDimensions()
        {
            return _context.Dimensions;
        }

        // GET: api/Dimension/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDimension([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dimension = await _context.Dimensions.FindAsync(id);

            if (dimension == null)
            {
                return NotFound();
            }

            return Ok(dimension);
        }

        // PUT: api/Dimension/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDimension([FromRoute] int id, [FromBody] Dimension dimension)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dimension.DimensionId)
            {
                return BadRequest();
            }

            _context.Entry(dimension).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DimensionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Dimension
        [HttpPost]
        public async Task<IActionResult> PostDimension([FromBody] Dimension dimension)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Dimensions.Add(dimension);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDimension", new { id = dimension.DimensionId }, dimension);
        }

        // DELETE: api/Dimension/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDimension([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dimension = await _context.Dimensions.FindAsync(id);
            if (dimension == null)
            {
                return NotFound();
            }

            _context.Dimensions.Remove(dimension);
            await _context.SaveChangesAsync();

            return Ok(dimension);
        }

        private bool DimensionExists(int id)
        {
            return _context.Dimensions.Any(e => e.DimensionId == id);
        }
    }
}