using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.DTO;
using SiC_GC.Models;

namespace SiC_GC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionController : ControllerBase
    {
        private readonly SiCContext _context;

        public CollectionController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/Collection
        [HttpGet]
        public IEnumerable<Collection> GetCollections()
        {
            return _context.Collections.Include(m => m.products);

        }

        // GET: api/Collection/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCollection([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var collection = await _context.Collections.FindAsync(id);

            if (collection == null)
            {
                return NotFound();
            }

            return Ok(collection);
        }

        // PUT: api/Collection/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCollection([FromRoute] int id, [FromBody] Collection collection)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != collection.CollectionId)
            {
                return BadRequest();
            }

            _context.Entry(collection).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CollectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Collection
        [HttpPost]
        public async Task<IActionResult> PostCollection([FromBody] CollectionDTO collectionDTO)
        {
            Collection collection = new Collection();

            collection.name = collectionDTO.name;
            collection.products = collectionDTO.products;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ICollection<ProductRelation> listaAuxiliarproducts = new List<ProductRelation>();

            if (collection.products != null)
            {
                foreach (ProductRelation p in collection.products)
                {

                    p.ChildProduct = await _context.Products
                                            .FirstOrDefaultAsync(m => m.ProductId == p.ChildProductId);

                    listaAuxiliarproducts.Add(p);


                }

                collection.products.Clear();

                foreach (ProductRelation p in listaAuxiliarproducts)
                {
                    collection.products.Add(p);
                }
            }

            _context.Collections.Add(collection);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCollection", new { id = collection.CollectionId }, collection);
        }

        // DELETE: api/Collection/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCollection([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var collection = await _context.Collections.FindAsync(id);
            if (collection == null)
            {
                return NotFound();
            }

            _context.Collections.Remove(collection);
            await _context.SaveChangesAsync();

            return Ok(collection);
        }

        private bool CollectionExists(int id)
        {
            return _context.Collections.Any(e => e.CollectionId == id);
        }
    }
}