using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Models;
using SiC_GC.DTO;

namespace SiC_GC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private readonly SiCContext _context;

        public CatalogController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/Catalog
        [HttpGet]
        public IEnumerable<Catalog> GetCatalogs()
        {
            return _context.Catalogs;
        }

        // GET: api/Catalog/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCatalog([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var catalog = await _context.Catalogs.FindAsync(id);

            if (catalog == null)
            {
                return NotFound();
            }

            return Ok(catalog);
        }

        // PUT: api/Catalog/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCatalog([FromRoute] int id, [FromBody] Catalog catalog)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != catalog.CatalogId)
            {
                return BadRequest();
            }

            _context.Entry(catalog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CatalogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Catalog
        [HttpPost]
        public async Task<IActionResult> PostCatalog([FromBody] CatalogDTO catalogDTO)
        {
            Catalog catalog = new Catalog();

            catalog.name = catalogDTO.name;
            catalog.products = catalogDTO.products;


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ICollection<ProductRelation> listaAuxiliarproducts = new List<ProductRelation>();

            if (catalog.products != null)
            {
                foreach (ProductRelation p in catalog.products)
                {

                    p.ChildProduct = await _context.Products
                                            .FirstOrDefaultAsync(m => m.ProductId == p.ChildProductId);

                    listaAuxiliarproducts.Add(p);
                }

                catalog.products.Clear();

                foreach (ProductRelation p in listaAuxiliarproducts)
                {
                    catalog.products.Add(p);
                }
            }

            _context.Catalogs.Add(catalog);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCatalog", new { id = catalog.CatalogId }, catalog);
        }

        // DELETE: api/Catalog/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCatalog([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var catalog = await _context.Catalogs.FindAsync(id);
            if (catalog == null)
            {
                return NotFound();
            }

            _context.Catalogs.Remove(catalog);
            await _context.SaveChangesAsync();

            return Ok(catalog);
        }

        private bool CatalogExists(int id)
        {
            return _context.Catalogs.Any(e => e.CatalogId == id);
        }
    }
}