using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Models;

namespace SiC_GC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductPriceController : ControllerBase
    {
        private readonly SiCContext _context;

        public ProductPriceController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/ProductPrice
        [HttpGet]
        public IEnumerable<ProductPrice> GetProductPrices()
        {
            return _context.ProductPrices;
        }

        // GET: api/ProductPrice/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductPrice([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productPrice = await _context.ProductPrices.FindAsync(id);

            if (productPrice == null)
            {
                return NotFound();
            }

            return Ok(productPrice);
        }

        // GET: api/ProductPrice/Product/5
        [HttpGet("Product/{id}")]
        public ProductPrice GetProductPriceByProduct([FromRoute] int id)
        {
            var today = DateTime.Today;
            ICollection<ProductPrice> productprices = _context.ProductPrices.Where(u => u.item.ProductId == id).OrderByDescending(u => u.applicableDate).ToList();

            foreach (ProductPrice p in productprices)
            {
                if (p.applicableDate.CompareTo(today) <= 0)
                {
                    return p;
                }
            }

            return new ProductPrice();
        }

        // PUT: api/ProductPrice/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProductPrice([FromRoute] int id, [FromBody] ProductPrice productPrice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productPrice.ProductPriceId)
            {
                return BadRequest();
            }

            _context.Entry(productPrice).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductPriceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProductPrice
        [HttpPost]
        public async Task<IActionResult> PostProductPrice([FromBody] ProductPrice productPrice)
        {
            var ppp = await _context.Products.FindAsync(productPrice.item.ProductId);

            productPrice.item = ppp;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ProductPrices.Add(productPrice);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProductPrice", new { id = productPrice.ProductPriceId }, productPrice);
        }

        // DELETE: api/ProductPrice/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductPrice([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productPrice = await _context.ProductPrices.FindAsync(id);
            if (productPrice == null)
            {
                return NotFound();
            }

            _context.ProductPrices.Remove(productPrice);
            await _context.SaveChangesAsync();

            return Ok(productPrice);
        }

        private bool ProductPriceExists(int id)
        {
            return _context.ProductPrices.Any(e => e.ProductPriceId == id);
        }

        // GET: api/ProductPrice/ByProduct/5
        [HttpGet("ByProduct/{id}")]
        public IEnumerable<ProductPrice> GetAllProductPricesByProduct([FromRoute] int id)
        {
            var today = DateTime.Today;
            ICollection<ProductPrice> productprices = _context.ProductPrices.Where(u => u.item.ProductId == id).ToList();

            return productprices;
        }
    }
}