using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Models;

namespace SiC_GC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaterialFinishPriceController : ControllerBase
    {
        private readonly SiCContext _context;

        public MaterialFinishPriceController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/MaterialFinishPrice
        [HttpGet]
        public IEnumerable<MaterialFinishPrice> GetMaterialFinishPrices()
        {
            return _context.MaterialFinishPrices.Include(m => m.item).ThenInclude(i => i.material);
        }

        // GET: api/MaterialFinishPrice/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMaterialFinishPrice([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var materialFinishPrice = await _context.MaterialFinishPrices.Include(m => m.item).ThenInclude(i => i.material)
                            .FirstOrDefaultAsync(i => i.MaterialFinishPriceId == id);


            if (materialFinishPrice == null)
            {
                return NotFound();
            }

            return Ok(materialFinishPrice);
        }

        // PUT: api/MaterialFinishPrice/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMaterialFinishPrice([FromRoute] int id, [FromBody] MaterialFinishPrice materialFinishPrice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != materialFinishPrice.MaterialFinishPriceId)
            {
                return BadRequest();
            }

            _context.Entry(materialFinishPrice).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaterialFinishPriceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MaterialFinishPrice
        [HttpPost]
        public async Task<IActionResult> PostMaterialFinishPrice([FromBody] MaterialFinishPrice materialFinishPrice)
        {
            var mfp = await _context.MaterialFinishes.FindAsync(materialFinishPrice.item.MaterialFinishId);

            materialFinishPrice.item = mfp;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.MaterialFinishPrices.Add(materialFinishPrice);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMaterialFinishPrice", new { id = materialFinishPrice.MaterialFinishPriceId }, materialFinishPrice);
        }

        // DELETE: api/MaterialFinishPrice/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMaterialFinishPrice([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var materialFinishPrice = await _context.MaterialFinishPrices.FindAsync(id);
            if (materialFinishPrice == null)
            {
                return NotFound();
            }

            _context.MaterialFinishPrices.Remove(materialFinishPrice);
            await _context.SaveChangesAsync();

            return Ok(materialFinishPrice);
        }

        private bool MaterialFinishPriceExists(int id)
        {
            return _context.MaterialFinishPrices.Any(e => e.MaterialFinishPriceId == id);
        }
    }
}