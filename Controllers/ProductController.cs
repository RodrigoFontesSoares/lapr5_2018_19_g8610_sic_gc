using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Models;
using SiC_GC.DTO;

namespace SiC_GC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly SiCContext _context;
        private ProductRepository productRepository;
        public ProductController(SiCContext context)
        {
            productRepository= new ProductRepository(context);
            _context=context;
        }

        // GET: api/Product
        [HttpGet]
        [Authorize]
        public IActionResult GetProducts()
        {
           var objectList = _context.Products
            .Select(o => new
            {
                o.ProductId,
                o.name,
                o.category,
                o.dimensions,
                products = o.products.Select(ot =>
                new
                {
                    ot.ChildProduct.name
                }).ToList(),
                possibleMaterialFinishes = o.possibleMaterialFinishes.Select(ot =>
                new
                {
                    ot.MaterialFinish.material,
                    ot.MaterialFinishId,
                    ot.MaterialFinish.name
                }).ToList()
            });
            // .Include(p => p.possibleMaterialFinishes)
            // .Include(p => p.products)
            // .Include(p => p.dimensions);
            return Ok(objectList);
        }

        // GET: api/Product/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> GetProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //         var product = await _context.Products
            //         .Where(p => p.ProductId == id)
            // .SelectMany(p => p.possibleMaterialFinishes)
            // .Select(pc => pc.Club);
            var product = await _context.Products
            .Select(o => new
            {
                o.ProductId,
                o.name,
                o.category,
                o.dimensions,
                products = o.products.Select(ot =>
                new
                {
                    ot.ChildProduct.name
                }).ToList(),
                possibleMaterialFinishes = o.possibleMaterialFinishes.Select(ot =>
                new
                {
                    ot.MaterialFinish.material,
                    ot.MaterialFinishId,
                    ot.MaterialFinish.name
                }).ToList()
            }).FirstOrDefaultAsync(p => p.ProductId == id);
            // .Include(p => p.possibleMaterialFinishes)
            // .Include(p => p.products)
            // .Include(p => p.dimensions)
            // .FirstOrDefaultAsync(p => p.ProductId == id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct([FromRoute] int id, [FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.ProductId)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Product
        [HttpPost]
        public async Task<IActionResult> PostProduct([FromBody] ProductDTO productDTO)
        {
            Product product = new Product();

            product.name = productDTO.name;
            product.possibleMaterialFinishes = productDTO.possibleMaterialFinishes;
            product.products = productDTO.products;
            product.dimensions = productDTO.dimensions;
            product.category = productDTO.category;


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ICollection<ProductRelation> listaAuxiliarproducts = new List<ProductRelation>();

            if (product.products != null)
            {
                foreach (ProductRelation p in product.products)
                {

                    p.ChildProduct = await _context.Products
                                            .FirstOrDefaultAsync(m => m.ProductId == p.ChildProductId);
                    p.ParentProduct = product;

                    listaAuxiliarproducts.Add(p);


                }

                product.products.Clear();

                foreach (ProductRelation p in listaAuxiliarproducts)
                {
                    product.products.Add(p);
                }
            }

            product.category = await _context.Categories
                                            .FirstOrDefaultAsync(m => m.CategoryId == productDTO.category.CategoryId);

            ICollection<ProductMaterialFinish> listaAuxiliar = new List<ProductMaterialFinish>();

            if (product.possibleMaterialFinishes != null)
            {
                foreach (ProductMaterialFinish mf in product.possibleMaterialFinishes)
                {

                    mf.MaterialFinish = await _context.MaterialFinishes.Include(m => m.material)
                                            .FirstOrDefaultAsync(m => m.MaterialFinishId == mf.MaterialFinishId);
                    mf.Product = product;
                    listaAuxiliar.Add(mf);


                }

                product.possibleMaterialFinishes.Clear();

                foreach (ProductMaterialFinish mf in listaAuxiliar)
                {
                    product.possibleMaterialFinishes.Add(mf);
                }
            }


            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            CreatedAtAction("GetProduct", new { id = product.ProductId }, product);

            return Ok(product);
        }

        // DELETE: api/Product/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return Ok(product);
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.ProductId == id);
        }
    }
}