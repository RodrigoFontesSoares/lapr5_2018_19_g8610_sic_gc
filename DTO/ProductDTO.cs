using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SiC_GC.Models;

namespace SiC_GC.DTO
{

    public class ProductDTO
    {
        public string name { get; set; }
        public virtual ICollection<ProductMaterialFinish> possibleMaterialFinishes { get; set; }
        public virtual ICollection<ProductRelation> products { get; set; }
        public Dimension dimensions { get; set; }
        public Category category { get; set; }
        public ProductDTO()
        { }

    }
}