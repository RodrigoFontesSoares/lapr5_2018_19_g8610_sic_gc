using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SiC_GC.Models;



namespace SiC_GC.DTO
{
    public class CatalogDTO
    {
        public string name { get; set; }
        public virtual ICollection<ProductRelation> products { get; set; }
        public CatalogDTO()
        { }

    }
}