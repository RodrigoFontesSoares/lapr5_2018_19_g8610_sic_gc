using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Models;

public class ProductRepository : ControllerBase
{
    private readonly SiCContext _context;
    public ProductRepository(SiCContext context)
    {
        _context = context;
    }
    public IActionResult getProducts()
    {
        var objectList = _context.Products
            .Select(o => new
            {
                o.ProductId,
                o.name,
                o.category,
                o.dimensions,
                products = o.products.Select(ot =>
                new
                {
                    ot.ChildProduct.name
                }).ToList(),
                possibleMaterialFinishes = o.possibleMaterialFinishes.Select(ot =>
                new
                {
                    ot.MaterialFinish.material,
                    ot.MaterialFinishId,
                    ot.MaterialFinish.name
                }).ToList()
            });
        // .Include(p => p.possibleMaterialFinishes)
        // .Include(p => p.products)
        // .Include(p => p.dimensions);
        return Ok(objectList);
    }
}