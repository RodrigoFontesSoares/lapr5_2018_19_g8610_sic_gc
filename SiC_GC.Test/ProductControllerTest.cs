using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Controllers;
using SiC_GC.DTO;
using SiC_GC.Models;
using Xunit;
using Moq;

namespace SiC.Test
{
    [Collection("Sequential")]
    public class ProductControllerTest
    {

        SiCContext context;
        ProductController controller;

        private void AddBootstrap()
        {
            Dimension dimension = new Dimension();

            dimension.minHeight = 95;
            dimension.maxHeight = 105;
            dimension.minDepth = 95;
            dimension.maxDepth = 105;
            dimension.minWidth = 95;
            dimension.maxWidth = 105;

            Category category = new Category();
            category.name = "Category1";

            Product product = new Product();
            product.name = "Product1";
            product.dimensions = dimension;
            product.category = category;
            product.products = new List<ProductRelation>();

            Product product2 = new Product();
            product2.name = "Product1_1";
            product2.products = new List<ProductRelation>();

            ProductRelation productRelation = new ProductRelation();
            productRelation.ChildProduct = product2;
            productRelation.ChildProductId = product2.ProductId;
            productRelation.ParentProduct = product;
            productRelation.ParentProductId = product.ProductId;

            product.products.Add(productRelation);
            product2.products.Add(productRelation);

            context.Dimensions.Add(dimension);
            context.Categories.Add(category);
            context.Products.Add(product);
            context.Products.Add(product2);
            context.ProductRelations.Add(productRelation);
            context.SaveChanges();
        }

        private void RemoveBootstrap()
        {
            context.ProductRelations.Remove(context.ProductRelations.LastAsync().Result);
            context.Products.Remove(context.Products.LastAsync().Result);
            context.Products.Remove(context.Products.LastAsync().Result);
            context.Categories.Remove(context.Categories.LastAsync().Result);
            context.Dimensions.Remove(context.Dimensions.LastAsync().Result);
            context.SaveChanges();
        }

        public ProductControllerTest()
        {

            var connection = "TESTDB";
            var options = new DbContextOptionsBuilder<SiCContext>()
            .UseInMemoryDatabase(connection)
            .Options;

            context = new SiCContext(options);
            controller = new ProductController(context);
        }

        [Fact]
        public async void TestPutProduct()
        {
            AddBootstrap();

            ProductDTO dto = new ProductDTO();
            dto.name = "Product2";
            dto.dimensions = context.Dimensions.FirstAsync().Result;
            dto.category = context.Categories.FirstAsync().Result;
            dto.products = new List<ProductRelation>();
            await controller.PostProduct(dto);

            List<Product> ps = new List<Product>();
            foreach (Product paux in context.Products)
            {
                ps.Add(paux);
            }

            Product product = context.Products.FindAsync(context.Products.LastAsync().Result.ProductId).Result;

            //product.ProductId = context.Products.LastAsync().Result.ProductId;
            var result1 = await controller.PutProduct(context.Products.LastAsync().Result.ProductId, product);
            Assert.IsType<NoContentResult>(result1);

            //Absurdly high ID, doesn't exist
            var result2 = await controller.PutProduct((int)1000, product);
            Assert.IsType<BadRequestResult>(result2);

            //Back to existing ID, but body is null, must be a BadRequest
            try
            {
                await controller.PutProduct((int)product.ProductId, null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            await controller.DeleteProduct(context.Products.LastAsync().Result.ProductId);

            RemoveBootstrap();
        }

        [Fact]
        public async Task TestPostProduct()
        {
            AddBootstrap();

            ProductDTO dto = new ProductDTO();
            dto.name = "Product2";
            dto.dimensions = context.Dimensions.FirstAsync().Result;
            dto.category = context.Categories.FirstAsync().Result;
            dto.products = new List<ProductRelation>();

            //Should work
            var result = await controller.PostProduct(dto);
            Assert.IsType<OkObjectResult>(result);

            var result3 = await controller.PostProduct(null);
            Assert.IsType<BadRequestResult>(result3);

            await controller.DeleteProduct(context.Products.LastAsync().Result.ProductId);

            RemoveBootstrap();
        }

        // //TODO
        // [Fact]
        // public async void TestGetProductByName()
        // {
        //     string name = "Product1";
        //     string wrongName = "asdas";

        //     var result = await controller.GetProductByName(name);
        //     Assert.IsType<OkObjectResult>(result);

        //     var result2 = await controller.GetProductByName(wrongName);
        //     Assert.IsType<NotFoundObjectResult>(result2);
        // }

        // [Fact]
        // public async void TestGetParentProduct()
        // {
        //     var result = await controller.getParentProductLocal((long)1);
        //     Assert.IsType<NotFoundResult>(result);
        // }

        [Fact]
        public async Task TestDeleteProduct()
        {
            AddBootstrap();

            ProductDTO dto = new ProductDTO();
            dto.name = "Product2";
            dto.dimensions = context.Dimensions.FirstAsync().Result;
            dto.category = context.Categories.FirstAsync().Result;
            dto.products = new List<ProductRelation>();

            await controller.PostProduct(dto);

            List<Product> products = new List<Product>();

            foreach (Product c in context.Products)
            {
                products.Add(c);
            }

            Product product = context.Products.FindAsync(context.Products.FirstAsync().Result.ProductId).Result;

            var result = await controller.DeleteProduct(context.Products.LastAsync().Result.ProductId);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.DeleteProduct((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestGetProductById()
        {
            AddBootstrap();

            //Should find
            var result = await controller.GetProduct(context.Products.FirstAsync().Result.ProductId);
            Assert.IsType<OkObjectResult>(result);

            //Should not find
            var result2 = await controller.GetProduct((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }
    }
}
