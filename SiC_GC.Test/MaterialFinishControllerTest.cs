using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC;
using Xunit;
using SiC_GC.Models;
using SiC_GC.Controllers;

namespace SiC.Test
{
    [Collection("Sequential")]
    public class MaterialFinishControllerTest
    {
        SiCContext context;
        MaterialFinishController controller;

        private void AddBootstrap()
        {
            Material material = new Material();
            material.name = "Madeira";

            MaterialFinish materialFinish = new MaterialFinish();
            materialFinish.name = "MaterialFinish1";
            materialFinish.material = material;

            context.Materials.Add(material);
            context.MaterialFinishes.Add(materialFinish);

            context.SaveChanges();
        }

        private void RemoveBootstrap()
        {
            context.MaterialFinishes.Remove(context.MaterialFinishes.LastAsync().Result);
            context.Materials.Remove(context.Materials.LastAsync().Result);
            context.SaveChanges();
        }

        public MaterialFinishControllerTest()
        {
            var connection = "Test";
            var options = new DbContextOptionsBuilder<SiCContext>()
            .UseInMemoryDatabase(connection)
            .Options;

            context = new SiCContext(options);
            controller = new MaterialFinishController(context);
        }

        [Fact]
        public async void TestGetMaterialFinishById()
        {
            AddBootstrap();

            //Should find
            var result = await controller.GetMaterialFinish(context.MaterialFinishes.FirstAsync().Result.MaterialFinishId);
            Assert.IsType<OkObjectResult>(result);

            //Should not find
            var result2 = await controller.GetMaterialFinish((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPutMaterialFinish()
        {
            AddBootstrap();

            MaterialFinish materialFinish = new MaterialFinish();
            materialFinish.name = "MaterialFinish1";
            materialFinish.material = context.Materials.FirstAsync().Result;
            await controller.PostMaterialFinish(materialFinish);

            List<MaterialFinish> mfs = new List<MaterialFinish>();
            foreach (MaterialFinish mfaux in context.MaterialFinishes)
            {
                mfs.Add(mfaux);
            }

            //material.MaterialId = context.MaterialFinishes.LastAsync().Result.MaterialFinishId;
            var result1 = await controller.PutMaterialFinish((int)materialFinish.MaterialFinishId, materialFinish);
            Assert.IsType<NoContentResult>(result1);

            //Absurdly high ID, doesn't exist
            var result2 = await controller.PutMaterialFinish((int)1000, materialFinish);
            Assert.IsType<BadRequestResult>(result2);

            //Back to existing ID, but body is null, must be a BadRequest
            try
            {
                await controller.PutMaterialFinish((int)materialFinish.MaterialFinishId, null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            await controller.DeleteMaterialFinish(context.MaterialFinishes.LastAsync().Result.MaterialFinishId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPostMaterialFinish()
        {
            AddBootstrap();

            MaterialFinish materialFinish = new MaterialFinish();
            materialFinish.name = "MaterialFinish2";
            materialFinish.material = context.Materials.FirstAsync().Result;

            var result = await controller.PostMaterialFinish(materialFinish);
            Assert.IsType<CreatedAtActionResult>(result);

            try
            {
                await controller.PostMaterialFinish(null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            MaterialFinish materialFinishNull = new MaterialFinish();
            var result3 = await controller.PostMaterialFinish(materialFinishNull);
            Assert.IsType<BadRequestResult>(result3);

            await controller.DeleteMaterialFinish(context.MaterialFinishes.LastAsync().Result.MaterialFinishId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestDeleteMaterialFinish()
        {
            AddBootstrap();

            MaterialFinish materialFinish = new MaterialFinish();
            materialFinish.name = "MaterialFinish2";
            materialFinish.material = context.Materials.FirstAsync().Result;
            await controller.PostMaterialFinish(materialFinish);

            List<MaterialFinish> mfs = new List<MaterialFinish>();
            foreach (MaterialFinish mfaux in context.MaterialFinishes)
            {
                mfs.Add(mfaux);
            }

            var result = await controller.DeleteMaterialFinish(context.MaterialFinishes.FirstAsync().Result.MaterialFinishId);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.DeleteMaterialFinish((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }
    }
}