using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Controllers;
using SiC_GC.Models;
using Xunit;


namespace SiC.Test
{
    [Collection("Sequential")]
    public class DimensionControllerTest
    {

        SiCContext context;
        DimensionController controller;

        private void AddBootstrap()
        {
            Dimension dimension = new Dimension();

            dimension.minHeight = 95;
            dimension.maxHeight = 105;
            dimension.minDepth = 95;
            dimension.maxDepth = 105;
            dimension.minWidth = 95;
            dimension.maxWidth = 105;

            context.Dimensions.Add(dimension);
            context.SaveChanges();
        }

        private void RemoveBootstrap()
        {
            context.Dimensions.Remove(context.Dimensions.LastAsync().Result);
            context.SaveChanges();
        }

        public DimensionControllerTest()
        {
            var connection = "TESTDB";
            var options = new DbContextOptionsBuilder<SiCContext>()
            .UseInMemoryDatabase(connection)
            .Options;

            context = new SiCContext(options);
            controller = new DimensionController(context);
        }

        [Fact]
        public async void TestGetDimensionById()
        {
            AddBootstrap();

            //ID exists
            var result = await controller.GetDimension(context.Dimensions.FirstAsync().Result.DimensionId);
            Assert.IsType<OkObjectResult>(result);

            //Id does not exist
            var result2 = await controller.GetDimension((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPutDimension()
        {
            AddBootstrap();

            Dimension dimension = new Dimension();

            dimension.minHeight = 45;
            dimension.maxHeight = 55;
            dimension.minDepth = 45;
            dimension.maxDepth = 55;
            dimension.minWidth = 45;
            dimension.maxWidth = 55;

            await controller.PostDimension(dimension);

            List<Dimension> ds = new List<Dimension>();
            foreach (Dimension daux in context.Dimensions)
            {
                ds.Add(daux);
            }

            //dimension.DimensionId = context.Dimensions.LastAsync().Result.DimensionId;
            var result1 = await controller.PutDimension((int)dimension.DimensionId, dimension);
            Assert.IsType<NoContentResult>(result1);

            //Absurdly high ID, doesn't exist
            var result2 = await controller.PutDimension((int)1000, dimension);
            Assert.IsType<BadRequestResult>(result2);

            //Back to existing ID, but body is null, must be a BadRequest
            try
            {
                await controller.PutDimension((int)dimension.DimensionId, null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            await controller.DeleteDimension(context.Dimensions.LastAsync().Result.DimensionId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPostDimension()
        {
            AddBootstrap();

            Dimension dimension = new Dimension();

            dimension.minHeight = 45;
            dimension.maxHeight = 55;
            dimension.minDepth = 45;
            dimension.maxDepth = 55;
            dimension.minWidth = 45;
            dimension.maxWidth = 55;

            Dimension dimension2 = new Dimension();

            dimension.minHeight = 0;
            dimension.maxHeight = 0;
            dimension.minDepth = 0;
            dimension.maxDepth = 0;
            dimension.minWidth = 0;
            dimension.maxWidth = 0;

            //Should add normally
            var result = await controller.PostDimension(dimension);
            Assert.IsType<OkObjectResult>(result);

            //Can't post a null object
            var result2 = await controller.PostDimension(null);
            Assert.IsType<BadRequestResult>(result2);

            //Can't add a dimension without measurements
            var result3 = await controller.PostDimension(dimension2);
            Assert.IsType<BadRequestResult>(result3);

            await controller.DeleteDimension(context.Dimensions.LastAsync().Result.DimensionId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestDeleteDimension()
        {
            AddBootstrap();

            Dimension d = new Dimension();

            d.minHeight = 45;
            d.maxHeight = 55;
            d.minDepth = 45;
            d.maxDepth = 55;
            d.minWidth = 45;
            d.maxWidth = 55;

            await controller.PostDimension(d);

            List<Dimension> ds = new List<Dimension>();
            foreach (Dimension daux in context.Dimensions)
            {
                ds.Add(daux);
            }

            var result = await controller.DeleteDimension(context.Dimensions.FirstAsync().Result.DimensionId);
            Assert.IsType<OkObjectResult>(result);
            var result2 = await controller.DeleteDimension((int)100980);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

    }
}
