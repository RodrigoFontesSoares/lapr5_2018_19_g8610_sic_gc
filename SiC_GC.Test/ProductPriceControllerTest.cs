using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC;
using Xunit;
using SiC_GC.Models;
using SiC_GC.Controllers;

namespace SiC.Test
{
    [Collection("Sequential")]
    public class ProductPriceControllerTest
    {
        SiCContext context;
        ProductPriceController controller;

        private void AddBootstrap()
        {
            Dimension dimension = new Dimension();

            dimension.minHeight = 95;
            dimension.maxHeight = 105;
            dimension.minDepth = 95;
            dimension.maxDepth = 105;
            dimension.minWidth = 95;
            dimension.maxWidth = 105;

            Category category = new Category();
            category.name = "Category1";

            Product product = new Product();
            product.name = "Product1";
            product.dimensions = dimension;
            product.category = category;
            product.products = new List<ProductRelation>();

            ProductPrice productPrice = new ProductPrice();
            productPrice.item = product;
            productPrice.value = 10.00;
            productPrice.applicableDate = DateTime.Today;

            context.Dimensions.Add(dimension);
            context.Categories.Add(category);
            context.Products.Add(product);
            context.ProductPrices.Add(productPrice);

            context.SaveChanges();
        }

        private void RemoveBootstrap()
        {
            context.ProductPrices.Remove(context.ProductPrices.LastAsync().Result);
            context.Products.Remove(context.Products.LastAsync().Result);
            context.Categories.Remove(context.Categories.LastAsync().Result);
            context.Dimensions.Remove(context.Dimensions.LastAsync().Result);
            context.SaveChanges();
        }

        public ProductPriceControllerTest()
        {
            var connection = "Test";
            var options = new DbContextOptionsBuilder<SiCContext>()
            .UseInMemoryDatabase(connection)
            .Options;

            context = new SiCContext(options);
            controller = new ProductPriceController(context);
        }

        [Fact]
        public async void TestGetProductPriceById()
        {
            AddBootstrap();

            //Should find
            var result = await controller.GetProductPrice(context.ProductPrices.FirstAsync().Result.ProductPriceId);
            Assert.IsType<OkObjectResult>(result);

            //Should not find
            var result2 = await controller.GetProductPrice((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestGetProductPriceByProduct()
        {
            AddBootstrap();

            ProductPrice productPricePast = new ProductPrice();
            productPricePast.item = context.Products.FirstAsync().Result;
            productPricePast.value = 30.00;
            productPricePast.applicableDate = DateTime.Today.Subtract(new System.TimeSpan(1, 0, 0, 0));

            await controller.PostProductPrice(productPricePast);

            ProductPrice productPriceFuture = new ProductPrice();
            productPriceFuture.item = context.Products.FirstAsync().Result;
            productPriceFuture.value = 20.00;
            productPriceFuture.applicableDate = DateTime.Today.Add(new System.TimeSpan(1, 0, 0, 0));

            await controller.PostProductPrice(productPriceFuture);

            //Should find
            ProductPrice resultPresent = controller.GetProductPriceByProduct(context.Products.FirstAsync().Result.ProductId);
            Assert.Equal(10.00, resultPresent.value);

            await controller.DeleteProductPrice(resultPresent.ProductPriceId);

            ProductPrice resultPast = controller.GetProductPriceByProduct(context.Products.FirstAsync().Result.ProductId);
            Assert.Equal(30.00, resultPast.value);

            await controller.DeleteProductPrice(resultPast.ProductPriceId);

            ProductPrice resultFuture = controller.GetProductPriceByProduct(context.Products.FirstAsync().Result.ProductId);
            Assert.Null(resultFuture);

            await controller.DeleteProductPrice(productPriceFuture.ProductPriceId);

            await controller.PostProductPrice(resultPresent);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPutProductPrice()
        {
            AddBootstrap();

            ProductPrice productPrice = new ProductPrice();
            productPrice.item = context.Products.FirstAsync().Result;
            productPrice.value = 20.00;
            productPrice.applicableDate = DateTime.Today.Add(new System.TimeSpan(1, 0, 0, 0));

            await controller.PostProductPrice(productPrice);

            List<ProductPrice> pps = new List<ProductPrice>();
            foreach (ProductPrice ppaux in context.ProductPrices)
            {
                pps.Add(ppaux);
            }

            //productPrice.ProductPriceId = context.ProductPrices.LastAsync().Result.ProductPriceId;
            var result1 = await controller.PutProductPrice((int)productPrice.ProductPriceId, productPrice);
            Assert.IsType<NoContentResult>(result1);

            //Absurdly high ID, doesn't exist
            var result2 = await controller.PutProductPrice((int)1000, productPrice);
            Assert.IsType<BadRequestResult>(result2);

            //Back to existing ID, but body is null, must be a BadRequest
            try
            {
                await controller.PutProductPrice((int)productPrice.ProductPriceId, null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            await controller.DeleteProductPrice(context.ProductPrices.LastAsync().Result.ProductPriceId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPostProductPrice()
        {
            AddBootstrap();

            ProductPrice productPrice = new ProductPrice();
            productPrice.item = context.Products.FirstAsync().Result;
            productPrice.value = 20.00;
            productPrice.applicableDate = DateTime.Today.Add(new System.TimeSpan(1, 0, 0, 0));

            var result = await controller.PostProductPrice(productPrice);
            Assert.IsType<CreatedAtActionResult>(result);

            try
            {
                await controller.PostProductPrice(null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            ProductPrice productPriceNull = new ProductPrice();
            var result3 = await controller.PostProductPrice(productPrice);
            Assert.IsType<BadRequestResult>(result3);

            await controller.DeleteProductPrice(context.ProductPrices.LastAsync().Result.ProductPriceId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestDeleteProductPrice()
        {
            AddBootstrap();

            ProductPrice productPrice = new ProductPrice();
            productPrice.item = context.Products.FirstAsync().Result;
            productPrice.value = 20.00;
            productPrice.applicableDate = DateTime.Today.Add(new System.TimeSpan(1, 0, 0, 0));

            await controller.PostProductPrice(productPrice);

            List<ProductPrice> pps = new List<ProductPrice>();
            foreach (ProductPrice ppaux in context.ProductPrices)
            {
                pps.Add(ppaux);
            }

            var result = await controller.DeleteProductPrice(context.ProductPrices.FirstAsync().Result.ProductPriceId);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.DeleteProductPrice((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }
    }
}