using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC;
using Xunit;
using SiC_GC.Models;
using SiC_GC.Controllers;

namespace SiC.Test
{
    [Collection("Sequential")]
    public class MaterialControllerTest
    {
        SiCContext context;
        MaterialController controller;

        private void AddBootstrap()
        {
            Material material = new Material();
            material.name = "Madeira";

            context.Materials.Add(material);

            context.SaveChanges();
        }

        private void RemoveBootstrap()
        {
            context.Materials.Remove(context.Materials.LastAsync().Result);
            context.SaveChanges();
        }

        public MaterialControllerTest()
        {
            var connection = "Test";
            var options = new DbContextOptionsBuilder<SiCContext>()
            .UseInMemoryDatabase(connection)
            .Options;

            context = new SiCContext(options);
            controller = new MaterialController(context);
        }

        [Fact]
        public async void TestGetMaterialById()
        {
            AddBootstrap();

            //Should find
            var result = await controller.GetMaterial(context.Materials.FirstAsync().Result.MaterialId);
            Assert.IsType<OkObjectResult>(result);

            //Should not find
            var result2 = await controller.GetMaterial((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPutMaterial()
        {
            AddBootstrap();

            Material material = new Material();
            material.name = "Material1";
            await controller.PostMaterial(material);

            List<Material> ms = new List<Material>();
            foreach (Material maux in context.Materials)
            {
                ms.Add(maux);
            }

            //material.MaterialId = context.Materials.LastAsync().Result.MaterialId;
            var result1 = await controller.PutMaterial((int)material.MaterialId, material);
            Assert.IsType<NoContentResult>(result1);

            //Absurdly high ID, doesn't exist
            var result2 = await controller.PutMaterial((int)1000, material);
            Assert.IsType<BadRequestResult>(result2);

            //Back to existing ID, but body is null, must be a BadRequest
            try
            {
                await controller.PutMaterial((int)material.MaterialId, null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            await controller.DeleteMaterial(context.Materials.LastAsync().Result.MaterialId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPostMaterial()
        {
            AddBootstrap();

            Material material = new Material();
            material.name = "Material2";

            var result = await controller.PostMaterial(material);
            Assert.IsType<CreatedAtActionResult>(result);

            try
            {
                await controller.PostMaterial(null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            Material materialNull = new Material();
            var result3 = await controller.PostMaterial(materialNull);
            Assert.IsType<BadRequestResult>(result3);

            await controller.DeleteMaterial(context.Materials.LastAsync().Result.MaterialId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestDeleteMaterial()
        {
            AddBootstrap();

            Material material = new Material();
            material.name = "Material2";
            await controller.PostMaterial(material);

            List<Material> ms = new List<Material>();
            foreach (Material maux in context.Materials)
            {
                ms.Add(maux);
            }

            var result = await controller.DeleteMaterial(context.Materials.FirstAsync().Result.MaterialId);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.DeleteMaterial((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }
    }
}