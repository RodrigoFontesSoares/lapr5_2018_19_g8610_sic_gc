using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC;
using Xunit;
using SiC_GC.Models;
using SiC_GC.Controllers;

namespace SiC.Test
{
    [Collection("Sequential")]
    public class MaterialFinishPriceControllerTest
    {
        SiCContext context;
        MaterialFinishPriceController controller;

        private void AddBootstrap()
        {
            Material material = new Material();
            material.name = "Madeira";

            MaterialFinish materialFinish = new MaterialFinish();
            materialFinish.name = "MaterialFinish1";
            materialFinish.material = material;

            MaterialFinishPrice materialFinishPrice = new MaterialFinishPrice();
            materialFinishPrice.item = materialFinish;
            materialFinishPrice.value = 1.00;
            materialFinishPrice.applicableDate = DateTime.Today;

            context.Materials.Add(material);
            context.MaterialFinishes.Add(materialFinish);
            context.MaterialFinishPrices.Add(materialFinishPrice);

            context.SaveChanges();
        }

        private void RemoveBootstrap()
        {
            context.MaterialFinishPrices.Remove(context.MaterialFinishPrices.LastAsync().Result);
            context.MaterialFinishes.Remove(context.MaterialFinishes.LastAsync().Result);
            context.Materials.Remove(context.Materials.LastAsync().Result);
            context.SaveChanges();
        }

        public MaterialFinishPriceControllerTest()
        {
            var connection = "Test";
            var options = new DbContextOptionsBuilder<SiCContext>()
            .UseInMemoryDatabase(connection)
            .Options;

            context = new SiCContext(options);
            controller = new MaterialFinishPriceController(context);
        }

        [Fact]
        public async void TestGetMaterialFinishPriceById()
        {
            AddBootstrap();

            //Should find
            var result = await controller.GetMaterialFinishPrice(context.MaterialFinishPrices.FirstAsync().Result.MaterialFinishPriceId);
            Assert.IsType<OkObjectResult>(result);

            //Should not find
            var result2 = await controller.GetMaterialFinishPrice((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPutMaterialFinishPrice()
        {
            AddBootstrap();

            MaterialFinishPrice materialFinishPrice = new MaterialFinishPrice();
            materialFinishPrice.item = context.MaterialFinishes.FirstAsync().Result;
            materialFinishPrice.value = 2.00;
            materialFinishPrice.applicableDate = DateTime.Today.Add(new System.TimeSpan(1, 0, 0, 0));

            await controller.PostMaterialFinishPrice(materialFinishPrice);

            List<MaterialFinishPrice> mfps = new List<MaterialFinishPrice>();
            foreach (MaterialFinishPrice mfpaux in context.MaterialFinishPrices)
            {
                mfps.Add(mfpaux);
            }

            //materialFinishPrice.MaterialFinishPriceId = context.MaterialFinishPrices.LastAsync().Result.MaterialFinishPriceId;
            var result1 = await controller.PutMaterialFinishPrice((int)materialFinishPrice.MaterialFinishPriceId, materialFinishPrice);
            Assert.IsType<NoContentResult>(result1);

            //Absurdly high ID, doesn't exist
            var result2 = await controller.PutMaterialFinishPrice((int)1000, materialFinishPrice);
            Assert.IsType<BadRequestResult>(result2);

            //Back to existing ID, but body is null, must be a BadRequest
            try
            {
                await controller.PutMaterialFinishPrice((int)materialFinishPrice.MaterialFinishPriceId, null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            await controller.DeleteMaterialFinishPrice(context.MaterialFinishPrices.LastAsync().Result.MaterialFinishPriceId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPostMaterialFinishPrice()
        {
            AddBootstrap();

            MaterialFinishPrice materialFinishPrice = new MaterialFinishPrice();
            materialFinishPrice.item = context.MaterialFinishes.FirstAsync().Result;
            materialFinishPrice.value = 2.00;
            materialFinishPrice.applicableDate = DateTime.Today.Add(new System.TimeSpan(1, 0, 0, 0));

            var result = await controller.PostMaterialFinishPrice(materialFinishPrice);
            Assert.IsType<CreatedAtActionResult>(result);

            try
            {
                await controller.PostMaterialFinishPrice(null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            MaterialFinishPrice materialFinishPriceNull = new MaterialFinishPrice();
            var result3 = await controller.PostMaterialFinishPrice(materialFinishPriceNull);
            Assert.IsType<BadRequestResult>(result3);

            await controller.DeleteMaterialFinishPrice(context.MaterialFinishPrices.LastAsync().Result.MaterialFinishPriceId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestDeleteMaterialFinishPrice()
        {
            AddBootstrap();

            MaterialFinishPrice materialFinishPrice = new MaterialFinishPrice();
            materialFinishPrice.item = context.MaterialFinishes.FirstAsync().Result;
            materialFinishPrice.value = 2.00;
            materialFinishPrice.applicableDate = DateTime.Today.Add(new System.TimeSpan(1, 0, 0, 0));

            await controller.PostMaterialFinishPrice(materialFinishPrice);

            List<MaterialFinishPrice> mfps = new List<MaterialFinishPrice>();
            foreach (MaterialFinishPrice mfpaux in context.MaterialFinishPrices)
            {
                mfps.Add(mfpaux);
            }

            var result = await controller.DeleteMaterialFinishPrice(context.MaterialFinishPrices.FirstAsync().Result.MaterialFinishPriceId);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.DeleteMaterialFinishPrice((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }
    }
}