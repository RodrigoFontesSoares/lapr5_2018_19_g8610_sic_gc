using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Controllers;
using SiC_GC.Models;
using Xunit;

namespace SiC.Test
{
    [Collection("Sequential")]
    public class CategoryControllerTest
    {

        SiCContext context;
        CategoryController controller;

        private void AddBootstrap()
        {
            Category category = new Category();

            category.name = "Category1";
            context.Categories.Add(category);
            context.SaveChanges();

            Category category2 = new Category();

            category2.name = "Category1_1";
            category2.father = category;
            context.Categories.Add(category2);

            context.SaveChanges();
        }

        private void RemoveBootstrap()
        {
            context.Categories.Remove(context.Categories.LastAsync().Result);
            context.SaveChanges();

            context.Categories.Remove(context.Categories.LastAsync().Result);
            context.SaveChanges();
        }

        public CategoryControllerTest()
        {
            var connection = "TESTDB";
            var options = new DbContextOptionsBuilder<SiCContext>()
            .UseInMemoryDatabase(connection)
            .Options;

            context = new SiCContext(options);
            controller = new CategoryController(context);
        }

        [Fact]
        public async void TestPutCategory()
        {
            AddBootstrap();

            Category category = new Category();
            category.name = "Category2";
            category.father = null;

            await controller.PostCategory(category);

            List<Category> cs = new List<Category>();
            foreach (Category caux in context.Categories)
            {
                cs.Add(caux);
            }

            //category.CategoryId = context.Categories.LastAsync().Result.CategoryId;
            var result1 = await controller.PutCategory((int)category.CategoryId, category);
            Assert.IsType<NoContentResult>(result1);

            //Absurdly high ID, doesn't exist
            var result2 = await controller.PutCategory((int)1000, category);
            Assert.IsType<BadRequestResult>(result2);

            //Back to existing ID, but body is null, must be a BadRequest
            try
            {
                await controller.PutCategory((int)category.CategoryId, null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            await controller.DeleteCategory(context.Categories.LastAsync().Result.CategoryId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPostCategory()
        {
            AddBootstrap();

            Category category = new Category();
            category.name = "Category2";

            //Should work
            var result = await controller.PostCategory(category);
            Assert.IsType<OkObjectResult>(result);

            //If name is null
            Category categoryNull = new Category();
            var result2 = await controller.PostCategory(categoryNull);
            Assert.IsType<BadRequestResult>(result2);

            var result3 = await controller.PostCategory(null);
            Assert.IsType<BadRequestResult>(result3);

            await controller.DeleteCategory(context.Categories.LastAsync().Result.CategoryId);

            RemoveBootstrap();
        }


        [Fact]
        public async void TestDeleteCategory()
        {
            AddBootstrap();

            Category category = new Category();
            category.name = "Category2";

            await controller.PostCategory(category);

            List<Category> categories = new List<Category>();

            foreach (Category c in context.Categories)
            {
                categories.Add(c);
            }

            var result = await controller.DeleteCategory(context.Categories.FirstAsync().Result.CategoryId);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.DeleteCategory((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestGetCategoryById()
        {
            AddBootstrap();

            var result = await controller.GetCategory(context.Categories.FirstAsync().Result.CategoryId);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.GetCategory((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

        // [Fact]
        // public async void GetAllTest()
        // {
        //     var result = await controller.GetCategories();
        //     Assert.IsType<OkObjectResult>(result);
        // }

    }
}
