﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Controllers;
using SiC_GC.DTO;
using SiC_GC.Models;
using Xunit;

namespace SiC.Test
{
    [Collection("Sequential")]
    public class CatalogControllerTest
    {

        SiCContext context;
        CatalogController controller;

        private void AddBootstrap()
        {
            Catalog catalog = new Catalog();
            catalog.name = "CatalogTest";

            context.Catalogs.Add(catalog);
            context.SaveChanges();
        }

        private void RemoveBootstrap()
        {
            context.Catalogs.Remove(context.Catalogs.LastAsync().Result);
            context.SaveChanges();
        }

        public CatalogControllerTest()
        {

            var connection = "TESTDB";
            var options = new DbContextOptionsBuilder<SiCContext>()
            .UseInMemoryDatabase(connection)
            .Options;

            context = new SiCContext(options);
            controller = new CatalogController(context);
        }

        [Fact]
        public async void TestGetCatalogById()
        {
            AddBootstrap();

            //Should find
            var result = await controller.GetCatalog(context.Catalogs.FirstAsync().Result.CatalogId);
            Assert.IsType<OkObjectResult>(result);

            //Should not find
            var result2 = await controller.GetCatalog((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPutCatalog()
        {
            AddBootstrap();

            CatalogDTO dto = new CatalogDTO();
            dto.name = "Catalog1";
            await controller.PostCatalog(dto);

            List<Catalog> cs = new List<Catalog>();
            foreach (Catalog caux in context.Catalogs)
            {
                cs.Add(caux);
            }

            Catalog catalog = context.Catalogs.FindAsync(context.Catalogs.LastAsync().Result.CatalogId).Result;

            //catalog.CatalogId = context.Catalogs.LastAsync().Result.CatalogId;
            var result1 = await controller.PutCatalog((int)catalog.CatalogId, catalog);
            Assert.IsType<NoContentResult>(result1);

            //Absurdly high ID, doesn't exist
            var result2 = await controller.PutCatalog((int)1000, catalog);
            Assert.IsType<BadRequestResult>(result2);

            //Back to existing ID, but body is null, must be a BadRequest
            try
            {
                await controller.PutCatalog((int)catalog.CatalogId, null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            await controller.DeleteCatalog(context.Catalogs.LastAsync().Result.CatalogId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPostCatalog()
        {
            AddBootstrap();

            CatalogDTO dto = new CatalogDTO();
            dto.name = "Catalog2";

            var result = await controller.PostCatalog(dto);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.PostCatalog(null);
            Assert.IsType<BadRequestResult>(result2);

            CatalogDTO dtoNameNull = new CatalogDTO();
            var result3 = await controller.PostCatalog(dtoNameNull);
            Assert.IsType<BadRequestResult>(result3);

            await controller.DeleteCatalog(context.Catalogs.LastAsync().Result.CatalogId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestDeleteCatalog()
        {
            AddBootstrap();

            CatalogDTO dto = new CatalogDTO();
            dto.name = "Catalog2";
            await controller.PostCatalog(dto);

            List<Catalog> cs = new List<Catalog>();
            foreach (Catalog caux in context.Catalogs)
            {
                cs.Add(caux);
            }

            var result = await controller.DeleteCatalog(context.Catalogs.FirstAsync().Result.CatalogId);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.DeleteCatalog((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }
    }
}
