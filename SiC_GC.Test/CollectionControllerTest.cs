﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiC_GC.Controllers;
using SiC_GC.DTO;
using SiC_GC.Models;
using Xunit;

namespace SiC.Test
{
    [Collection("Sequential")]
    public class CollectionControllerTest
    {

        SiCContext context;
        CollectionController controller;

        private void AddBootstrap()
        {
            Collection collection = new Collection();
            collection.name = "CollectionTest";

            context.Collections.Add(collection);
            context.SaveChanges();
        }

        private void RemoveBootstrap()
        {
            context.Collections.Remove(context.Collections.LastAsync().Result);
            context.SaveChanges();
        }

        public CollectionControllerTest()
        {
            var connection = "TESTDB";
            var options = new DbContextOptionsBuilder<SiCContext>()
            .UseInMemoryDatabase(connection)
            .Options;

            context = new SiCContext(options);
            controller = new CollectionController(context);
        }

        [Fact]
        public async void TestGetCollectionById()
        {
            AddBootstrap();

            //Should find
            var result = await controller.GetCollection(context.Collections.FirstAsync().Result.CollectionId);
            Assert.IsType<OkObjectResult>(result);

            //Should not find
            var result2 = await controller.GetCollection((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPutCollection()
        {
            AddBootstrap();

            CollectionDTO dto = new CollectionDTO();
            dto.name = "Collection1";
            await controller.PostCollection(dto);

            List<Collection> cs = new List<Collection>();
            foreach (Collection caux in context.Collections)
            {
                cs.Add(caux);
            }

            Collection collection = context.Collections.FindAsync(context.Collections.LastAsync().Result.CollectionId).Result;

            //collection.CollectionId = context.Collections.LastAsync().Result.CollectionId;
            var result1 = await controller.PutCollection((int)collection.CollectionId, collection);
            Assert.IsType<NoContentResult>(result1);

            //Absurdly high ID, doesn't exist
            var result2 = await controller.PutCollection((int)1000, collection);
            Assert.IsType<BadRequestResult>(result2);

            //Back to existing ID, but body is null, must be a BadRequest
            try
            {
                await controller.PutCollection((int)collection.CollectionId, null);
            }
            catch (System.ArgumentNullException)
            {
                Assert.True(true);
            }
            catch (System.NullReferenceException)
            {
                Assert.True(true);
            }

            await controller.DeleteCollection(context.Collections.LastAsync().Result.CollectionId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestPostCollection()
        {
            AddBootstrap();

            CollectionDTO dto = new CollectionDTO();
            dto.name = "Collection2";

            var result = await controller.PostCollection(dto);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.PostCollection(null);
            Assert.IsType<BadRequestResult>(result2);

            CollectionDTO dtoNameNull = new CollectionDTO();
            var result3 = await controller.PostCollection(dtoNameNull);
            Assert.IsType<BadRequestResult>(result3);

            await controller.DeleteCollection(context.Collections.LastAsync().Result.CollectionId);

            RemoveBootstrap();
        }

        [Fact]
        public async void TestDeleteCollection()
        {
            AddBootstrap();

            CollectionDTO dto = new CollectionDTO();
            dto.name = "Collection2";
            await controller.PostCollection(dto);

            List<Collection> cs = new List<Collection>();
            foreach (Collection caux in context.Collections)
            {
                cs.Add(caux);
            }

            var result = await controller.DeleteCollection(context.Collections.FirstAsync().Result.CollectionId);
            Assert.IsType<OkObjectResult>(result);

            var result2 = await controller.DeleteCollection((int)1000);
            Assert.IsType<NotFoundResult>(result2);

            RemoveBootstrap();
        }
    }
}
