﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SiC_GC.Models;

namespace SiC_GC.Migrations
{
    [DbContext(typeof(SiCContext))]
    [Migration("20190103151825_colletionState")]
    partial class colletionState
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.3-rtm-32065")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ProductMaterialFinish", b =>
                {
                    b.Property<int>("ProductId");

                    b.Property<int>("MaterialFinishId");

                    b.HasKey("ProductId", "MaterialFinishId");

                    b.HasIndex("MaterialFinishId");

                    b.ToTable("ProductMaterialFinishes");
                });

            modelBuilder.Entity("ProductRelation", b =>
                {
                    b.Property<int>("productRelationId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("ChildProductId");

                    b.Property<int?>("CollectionId");

                    b.Property<int?>("ParentProductId");

                    b.Property<int?>("ProductId");

                    b.HasKey("productRelationId");

                    b.HasIndex("ChildProductId");

                    b.HasIndex("CollectionId");

                    b.HasIndex("ParentProductId");

                    b.HasIndex("ProductId");

                    b.ToTable("ProductRelations");
                });

            modelBuilder.Entity("SiC_GC.Models.Catalog", b =>
                {
                    b.Property<int>("CatalogId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("name");

                    b.HasKey("CatalogId");

                    b.ToTable("Catalogs");
                });

            modelBuilder.Entity("SiC_GC.Models.Category", b =>
                {
                    b.Property<int>("CategoryId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("fatherCategoryId");

                    b.Property<string>("name");

                    b.HasKey("CategoryId");

                    b.HasIndex("fatherCategoryId");

                    b.ToTable("Categories");
                });

            modelBuilder.Entity("SiC_GC.Models.Collection", b =>
                {
                    b.Property<int>("CollectionId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("name");

                    b.HasKey("CollectionId");

                    b.ToTable("Collections");
                });

            modelBuilder.Entity("SiC_GC.Models.Dimension", b =>
                {
                    b.Property<int>("DimensionId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<double>("maxDepth");

                    b.Property<double>("maxHeight");

                    b.Property<double>("maxWidth");

                    b.Property<double>("minDepth");

                    b.Property<double>("minHeight");

                    b.Property<double>("minWidth");

                    b.HasKey("DimensionId");

                    b.ToTable("Dimensions");
                });

            modelBuilder.Entity("SiC_GC.Models.Material", b =>
                {
                    b.Property<int>("MaterialId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("name");

                    b.HasKey("MaterialId");

                    b.ToTable("Materials");
                });

            modelBuilder.Entity("SiC_GC.Models.MaterialFinish", b =>
                {
                    b.Property<int>("MaterialFinishId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("MaterialId");

                    b.Property<string>("name");

                    b.HasKey("MaterialFinishId");

                    b.HasIndex("MaterialId");

                    b.ToTable("MaterialFinishes");
                });

            modelBuilder.Entity("SiC_GC.Models.MaterialFinishPrice", b =>
                {
                    b.Property<int>("MaterialFinishPriceId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("applicableDate");

                    b.Property<int?>("itemMaterialFinishId");

                    b.Property<double>("value");

                    b.HasKey("MaterialFinishPriceId");

                    b.HasIndex("itemMaterialFinishId");

                    b.ToTable("MaterialFinishPrices");
                });

            modelBuilder.Entity("SiC_GC.Models.Product", b =>
                {
                    b.Property<int>("ProductId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("CatalogId");

                    b.Property<int?>("CategoryId");

                    b.Property<int?>("dimensionsDimensionId");

                    b.Property<string>("name");

                    b.HasKey("ProductId");

                    b.HasIndex("CatalogId");

                    b.HasIndex("CategoryId");

                    b.HasIndex("dimensionsDimensionId");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("SiC_GC.Models.ProductPrice", b =>
                {
                    b.Property<int>("ProductPriceId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("applicableDate");

                    b.Property<int?>("itemProductId");

                    b.Property<double>("value");

                    b.HasKey("ProductPriceId");

                    b.HasIndex("itemProductId");

                    b.ToTable("ProductPrices");
                });

            modelBuilder.Entity("ProductMaterialFinish", b =>
                {
                    b.HasOne("SiC_GC.Models.MaterialFinish", "MaterialFinish")
                        .WithMany("ProductMaterialFinishes")
                        .HasForeignKey("MaterialFinishId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SiC_GC.Models.Product", "Product")
                        .WithMany("possibleMaterialFinishes")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ProductRelation", b =>
                {
                    b.HasOne("SiC_GC.Models.Product", "ChildProduct")
                        .WithMany()
                        .HasForeignKey("ChildProductId");

                    b.HasOne("SiC_GC.Models.Collection")
                        .WithMany("products")
                        .HasForeignKey("CollectionId");

                    b.HasOne("SiC_GC.Models.Product", "ParentProduct")
                        .WithMany()
                        .HasForeignKey("ParentProductId");

                    b.HasOne("SiC_GC.Models.Product")
                        .WithMany("products")
                        .HasForeignKey("ProductId");
                });

            modelBuilder.Entity("SiC_GC.Models.Category", b =>
                {
                    b.HasOne("SiC_GC.Models.Category", "father")
                        .WithMany()
                        .HasForeignKey("fatherCategoryId");
                });

            modelBuilder.Entity("SiC_GC.Models.MaterialFinish", b =>
                {
                    b.HasOne("SiC_GC.Models.Material", "material")
                        .WithMany()
                        .HasForeignKey("MaterialId");
                });

            modelBuilder.Entity("SiC_GC.Models.MaterialFinishPrice", b =>
                {
                    b.HasOne("SiC_GC.Models.MaterialFinish", "item")
                        .WithMany()
                        .HasForeignKey("itemMaterialFinishId");
                });

            modelBuilder.Entity("SiC_GC.Models.Product", b =>
                {
                    b.HasOne("SiC_GC.Models.Catalog")
                        .WithMany("products")
                        .HasForeignKey("CatalogId");

                    b.HasOne("SiC_GC.Models.Category", "category")
                        .WithMany()
                        .HasForeignKey("CategoryId");

                    b.HasOne("SiC_GC.Models.Dimension", "dimensions")
                        .WithMany()
                        .HasForeignKey("dimensionsDimensionId");
                });

            modelBuilder.Entity("SiC_GC.Models.ProductPrice", b =>
                {
                    b.HasOne("SiC_GC.Models.Product", "item")
                        .WithMany()
                        .HasForeignKey("itemProductId");
                });
#pragma warning restore 612, 618
        }
    }
}
