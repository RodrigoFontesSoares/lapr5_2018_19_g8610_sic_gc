﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC_GC.Migrations
{
    public partial class InitialCatalog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Catalogs_CatalogId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_CatalogId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CatalogId",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "CatalogId",
                table: "ProductRelations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductRelations_CatalogId",
                table: "ProductRelations",
                column: "CatalogId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductRelations_Catalogs_CatalogId",
                table: "ProductRelations",
                column: "CatalogId",
                principalTable: "Catalogs",
                principalColumn: "CatalogId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductRelations_Catalogs_CatalogId",
                table: "ProductRelations");

            migrationBuilder.DropIndex(
                name: "IX_ProductRelations_CatalogId",
                table: "ProductRelations");

            migrationBuilder.DropColumn(
                name: "CatalogId",
                table: "ProductRelations");

            migrationBuilder.AddColumn<int>(
                name: "CatalogId",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_CatalogId",
                table: "Products",
                column: "CatalogId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Catalogs_CatalogId",
                table: "Products",
                column: "CatalogId",
                principalTable: "Catalogs",
                principalColumn: "CatalogId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
