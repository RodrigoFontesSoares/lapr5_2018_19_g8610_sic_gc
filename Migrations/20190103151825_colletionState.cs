﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SiC_GC.Migrations
{
    public partial class colletionState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Collections_CollectionId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_CollectionId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CollectionId",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "CollectionId",
                table: "ProductRelations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductRelations_CollectionId",
                table: "ProductRelations",
                column: "CollectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductRelations_Collections_CollectionId",
                table: "ProductRelations",
                column: "CollectionId",
                principalTable: "Collections",
                principalColumn: "CollectionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductRelations_Collections_CollectionId",
                table: "ProductRelations");

            migrationBuilder.DropIndex(
                name: "IX_ProductRelations_CollectionId",
                table: "ProductRelations");

            migrationBuilder.DropColumn(
                name: "CollectionId",
                table: "ProductRelations");

            migrationBuilder.AddColumn<int>(
                name: "CollectionId",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_CollectionId",
                table: "Products",
                column: "CollectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Collections_CollectionId",
                table: "Products",
                column: "CollectionId",
                principalTable: "Collections",
                principalColumn: "CollectionId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
