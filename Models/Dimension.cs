using System;
using System.Collections.Generic;

namespace SiC_GC.Models
{
    public class Dimension
    {
        public int DimensionId { get; set; }
        public double minHeight { get; set; }
        public double maxHeight { get; set; }
        public double minDepth { get; set; }
        public double maxDepth { get; set; }
        public double minWidth { get; set; }
        public double maxWidth { get; set; }

        public Dimension()
        {

        }

    }
}