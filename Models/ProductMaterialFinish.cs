using Newtonsoft.Json;
using SiC_GC.Models;

public class ProductMaterialFinish
{
    public int ProductId { get; set; }
    [JsonIgnore]
    public Product Product { get; set; }
    public int MaterialFinishId { get; set; }
    [JsonIgnore]
    public MaterialFinish MaterialFinish { get; set; }
}