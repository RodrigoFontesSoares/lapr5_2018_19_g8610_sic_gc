using System;

namespace SiC_GC.Models
{
    public interface Constraint
    {
        Boolean validate(Product target);

    }
}