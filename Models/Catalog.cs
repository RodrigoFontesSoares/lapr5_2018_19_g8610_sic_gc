using System;
using System.Collections.Generic;

namespace SiC_GC.Models
{
    public class Catalog
    {
        public int CatalogId { get; set; }
        public string name { get; set; }
        public ICollection<ProductRelation> products { get; set; }

        public Catalog()
        {

        }

    }
}