using System;

namespace SiC_GC.Models
{
    public class ProductPrice
    {
        public int ProductPriceId { get; set; }
        public double value { get; set; }
        public Product item { get; set; }
        public DateTime applicableDate { get; set; }
    }
}