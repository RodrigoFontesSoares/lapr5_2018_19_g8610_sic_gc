using System;

namespace SiC_GC.Models
{
    public class MaterialFinishPrice
    {
        public int MaterialFinishPriceId { get; set; }
        public double value { get; set; }
        public MaterialFinish item { get; set; }
        public DateTime applicableDate { get; set; }
    }
}