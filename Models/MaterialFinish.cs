using System;
using System.Collections.Generic;

namespace SiC_GC.Models
{
    public class MaterialFinish
    {
        public int MaterialFinishId { get; set; }
        public string name { get; set; }
        public Material material { get; set; }
        public virtual ICollection<ProductMaterialFinish> ProductMaterialFinishes { get; set; }

        public MaterialFinish()
        {

        }

    }
}