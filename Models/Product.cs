using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiC_GC.Models
{

    public class Product
    {

        public int ProductId { get; set; }
        public string name { get; set; }
        public virtual ICollection<ProductMaterialFinish> possibleMaterialFinishes { get; set; }
        public virtual ICollection<ProductRelation> products { get; set; }
        public Dimension dimensions { get; set; }
        public Category category { get; set; }
        public Product()
        { }

    }
}