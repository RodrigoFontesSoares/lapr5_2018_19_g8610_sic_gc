using System;
using System.Collections.Generic;

namespace SiC_GC.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string name { get; set; }
        public Category father { get; set; }
        
        public Category()
        {

        }

    }
}