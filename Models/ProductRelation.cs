using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using SiC_GC.Models;

public class ProductRelation
{
    public int productRelationId { get; set; }
    [JsonIgnore]
    public Product ParentProduct { get; set; }
    public int? ParentProductId { get; set; }
    [JsonIgnore]
    public Product ChildProduct { get; set; }
    public int? ChildProductId { get; set; }
}