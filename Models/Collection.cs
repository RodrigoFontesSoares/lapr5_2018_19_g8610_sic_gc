using System;
using System.Collections.Generic;

namespace SiC_GC.Models
{
    public class Collection
    {
        public int CollectionId { get; set; }
        public string name { get; set; }
        public ICollection<ProductRelation> products { get; set; }
        public Collection()
        {

        }

    }
}